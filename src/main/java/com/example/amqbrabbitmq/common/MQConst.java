package com.example.amqbrabbitmq.common;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 17:11 2019/10/21
 * </P>
 **/
public class   MQConst {

    public final static String TOPIC_QUEUENAME1 = "topic_queuename1";
    public final static String TOPIC_QUEUENAME2 = "topic_queuename2";
    public final static String TOPIC_EXCHANGE = "topic_exchange";
    public final static String TOPIC_KEY1="topicKey.A";
    /**
     * topic和direct最大的却别在这里
     * direct这里routingkey完全相同，所以不建议使用通配符，而topic可以使用通配符，如使用'*','#'
     * 如下，使用#：
     *              表示将队列绑定到所有topicKey.开头的routingKey
     */
    public final static String TOPIC_KEYS="topicKey.#";

    public final static String FANOUT_QUEUENAME1="fanout_queuename1";
    public final static String FANOUT_QUEUENAME2="fanout_queuename2";
    public final static String FANOUT_EXCHANGE="fanout_exchange";

}
