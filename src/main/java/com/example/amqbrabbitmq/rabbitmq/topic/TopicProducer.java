package com.example.amqbrabbitmq.rabbitmq.topic;

import com.example.amqbrabbitmq.common.MQConst;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：生成者
 * @date：crealed in 17:21 2019/10/21
 * </P>
 **/
@Configuration
public class TopicProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    //@Scheduled(fixedDelay = 10000L)
    public void send() {
        rabbitTemplate.convertAndSend(MQConst.TOPIC_EXCHANGE, MQConst.TOPIC_KEYS, "我是TOPIC_KEYS");
        rabbitTemplate.convertAndSend(MQConst.TOPIC_EXCHANGE, MQConst.TOPIC_KEY1, "我是TOPIC_KEY1");
    }
}
