package com.example.amqbrabbitmq.rabbitmq.topic;

import com.example.amqbrabbitmq.common.MQConst;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：消费者
 * @date：crealed in 17:20 2019/10/21
 * </P>
 **/
@Component
public class TopicConsumer {

    @RabbitHandler
    @RabbitListener(queues = MQConst.TOPIC_QUEUENAME1)
    public void process1(Object message) {
        System.out.println("queue:topic.message1,message:" + message);
    }

    @RabbitHandler
    @RabbitListener(queues = MQConst.TOPIC_QUEUENAME2)
    public void process2(Object message) {
        System.out.println("queue:topic.message2,message:" + message);
    }
}
