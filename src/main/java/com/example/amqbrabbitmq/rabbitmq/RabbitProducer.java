package com.example.amqbrabbitmq.rabbitmq;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：创建消息生产者
 * @date：crealed in 16:34 2019/10/21
 * </P>
 **/
@Component
public class RabbitProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    //@Scheduled(fixedDelay = 10000L)
    public void send() {
        rabbitTemplate.convertAndSend("zzm-rabbitmq", "Hello rabbitmq !~");
    }
}
