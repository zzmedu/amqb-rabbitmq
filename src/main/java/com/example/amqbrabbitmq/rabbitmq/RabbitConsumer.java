package com.example.amqbrabbitmq.rabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：创建消费者
 * @date：crealed in 16:35 2019/10/21
 * </P>
 **/
@Component
public class RabbitConsumer {

    @RabbitHandler
    @RabbitListener(queues = "zzm-rabbitmq")
    public void process(@Payload String foo) {
        System.out.println(new Date() + ": " + foo);
    }

}
