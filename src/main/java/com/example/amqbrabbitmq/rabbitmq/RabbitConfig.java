package com.example.amqbrabbitmq.rabbitmq;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：注入队列
 * @date：crealed in 16:33 2019/10/21
 * </P>
 **/
@Configuration
public class RabbitConfig {

    @Bean
    public Queue queue(){
        return new Queue("zzm-rabbitmq");
    }
}
