package com.example.amqbrabbitmq.rabbitmq.fanout;

import com.example.amqbrabbitmq.common.MQConst;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 17:55 2019/10/21
 * </P>
 **/
@Configuration
public class FanoutProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Scheduled(fixedDelay = 10000L)
    public void send() {
        rabbitTemplate.convertAndSend(MQConst.FANOUT_EXCHANGE,"", "我是FANOUT_QUEUENAME2");
    }
}
