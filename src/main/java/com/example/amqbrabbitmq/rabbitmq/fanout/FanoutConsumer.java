package com.example.amqbrabbitmq.rabbitmq.fanout;

import com.example.amqbrabbitmq.common.MQConst;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 17:52 2019/10/21
 * </P>
 **/
@Component
public class FanoutConsumer {

    @RabbitHandler
    @RabbitListener(queues = MQConst.FANOUT_QUEUENAME1)
    public void process1(String message){
        System.out.println("queue:fanout.message1,message:" + message);
    }

    @RabbitHandler
    @RabbitListener(queues = MQConst.FANOUT_QUEUENAME2)
    public void process2(String message){
        System.out.println("queue:fanout.message2,message:" + message);
    }
}
